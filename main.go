package main

import (
	"calculator_api/api"

	"github.com/gofiber/fiber/v2"
)

func setupRoutes(app *fiber.App) {
	app.Get("/", func(c *fiber.Ctx) error {
		return c.Status(fiber.StatusOK).JSON(fiber.Map{
			"success": true,
			"message": "This is your endpoint",
		})
	})

	app.Post("/api/add", api.Add)
	app.Post("/api/sub", api.Sub)
	app.Post("/api/mul", api.Mul)
	app.Post("/api/div", api.Div)
}

func main() {
	app := fiber.New()

	setupRoutes(app)

	err := app.Listen(":8000")

	// handle error
	if err != nil {
		panic(err)
	}

}
