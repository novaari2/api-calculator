package api

import (
	"github.com/gofiber/fiber/v2"
)

func Add(c *fiber.Ctx) error {
	payload := struct {
		Number1 int `binding:"required"`
		Number2 int `binding:"required"`
	}{}

	err := c.BodyParser(&payload)

	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"success": false,
			"message": "Error Calculate",
		})
	}
	hasil := payload.Number1 + payload.Number2
	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"success": true,
		"data":    hasil,
	})
}

func Sub(c *fiber.Ctx) error {
	payload := struct {
		Number1 int
		Number2 int
	}{}

	err := c.BodyParser(&payload)

	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"success": false,
			"message": "Error Calculate",
		})
	}
	hasil := payload.Number1 - payload.Number2
	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"success": true,
		"data":    hasil,
	})
}

func Mul(c *fiber.Ctx) error {
	payload := struct {
		Number1 int
		Number2 int
	}{}

	err := c.BodyParser(&payload)

	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"success": false,
			"message": "Error Calculate",
		})
	}
	hasil := payload.Number1 * payload.Number2
	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"success": true,
		"data":    hasil,
	})
}

func Div(c *fiber.Ctx) error {
	payload := struct {
		Number1 int
		Number2 int
	}{}

	err := c.BodyParser(&payload)

	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"success": false,
			"message": "Error Calculate",
		})
	}
	hasil := payload.Number1 / payload.Number2
	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"success": true,
		"data":    hasil,
	})
}
